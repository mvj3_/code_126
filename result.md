### 20130715
*   平均大小：在日志collection运行stats()命令的结果是 "avgObjSize" : 150.0414732684895。

### 20130325
*   平均大小：在日志collection运行stats()命令的结果是 "avgObjSize" : 147.15624621293335。

### 20130228
*   平均大小：在日志collection运行stats()命令的结果是 "avgObjSize" : 147.47498033253484。

### 20130126
*   平均大小：在日志collection运行stats()命令的结果是 "avgObjSize" : 148.07122050207835 。
*   包含信息：_id, 浏览器和版本信息，操作系统，子域名，当前URL，IP，HTTP referer, 用户ID, 访问时间等十个信息，平均每个信息大小为14.8比特。
*   待优化：HTTP referer字符串太长，_id和时间部分重复。